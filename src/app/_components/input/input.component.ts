import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {

  @Input() label!: string;
  @Input() field!: string;
  @Input() placeholder!: string;
  @Input() type: string = '';
  @Output() value: EventEmitter<string> = new EventEmitter<string>();
  isPassword!: boolean;

    constructor(){}

    ngOnInit(): void {
      this.isPassword = this.type == 'password';
    }

    togglePassword() {

      if(this.type == 'text'){
        this.type = 'password';
      }
      else {
        this.type = 'text';
      }
    }
}
